﻿Public Class CreditoPersonal
    Inherits Credito

    Public Sub New(c As Cliente)
        MyBase.New(c)
    End Sub

    Public Overrides Sub VerificarAcciones()
        Console.WriteLine("No es requerido verificar acciones para un crédito personal")
    End Sub

    Public Overrides Sub VerificarBalance()
        Console.WriteLine("Verificando balance bancario para un prestamo personal")
    End Sub

    Public Overrides Sub VerificarCreditos()
        Console.WriteLine("Verificando otros creditos para para un prestamo personal")
    End Sub

    Public Overrides Sub VerificarIngresos()
        Console.WriteLine("Verificando ingresos para asignar un prestamo personal")
    End Sub


End Class

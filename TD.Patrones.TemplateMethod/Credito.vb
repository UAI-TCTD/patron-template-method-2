﻿Public MustInherit Class Credito
    Inherits Verificaciones


    Public Sub New(c As Cliente)
        MyBase._cliente = c
    End Sub

    Public MustOverride Overrides Sub VerificarAcciones()
    Public MustOverride Overrides Sub VerificarBalance()
    Public MustOverride Overrides Sub VerificarIngresos()
    Public MustOverride Overrides Sub VerificarCreditos()

End Class

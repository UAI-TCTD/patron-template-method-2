﻿Public Class CreditoHipotecario
    Inherits Credito
    Public Sub New(c As Cliente)
        MyBase.New(c)
    End Sub

    Public Overrides Sub VerificarAcciones()
        Console.WriteLine("Verificando acciones para asignar un crédito hipotecario")
    End Sub

    Public Overrides Sub VerificarBalance()
        Console.WriteLine("Verificando balance bancario para un prestamo hipotecario")
    End Sub

    Public Overrides Sub VerificarCreditos()
        Console.WriteLine("Verificando otros creditos para para un prestamo hipotecario")
    End Sub

    Public Overrides Sub VerificarIngresos()
        Console.WriteLine("Verificando ingresos para asignar un prestamo hipotecario")
    End Sub

End Class

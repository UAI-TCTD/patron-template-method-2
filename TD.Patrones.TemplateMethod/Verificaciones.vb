﻿Public MustInherit Class Verificaciones
    Protected _cliente As Cliente


    Public Sub Verificar()
        Console.WriteLine(String.Format("Verificando crédito para {0}", _cliente.Nombre))
        VerificarAcciones()
        VerificarBalance()
        VerificarCreditos()
        VerificarIngresos()
    End Sub

    Public MustOverride Sub VerificarAcciones()
    Public MustOverride Sub VerificarBalance()
    Public MustOverride Sub VerificarCreditos()
    Public MustOverride Sub VerificarIngresos()
End Class

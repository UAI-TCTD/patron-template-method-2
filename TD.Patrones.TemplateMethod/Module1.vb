﻿Module Module1

    Sub Main()
        Dim c As New Cliente
        c.Nombre = "Pepe"

        Dim credito As Credito
        credito = New CreditoHipotecario(c)
        credito.Verificar()

        credito = New CreditoPersonal(c)
        credito.Verificar()


        Console.ReadKey()
    End Sub

End Module
